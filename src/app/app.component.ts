import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Task 29';
  public message: string = 'Hello Mickey Mouse'
  public numMessage: number = 1337
  public things: string[] = [
    ' Beer',
    ' More Beer',
    ' MOOOORE Beer'
  ];

  public arr: Array<object> = [
    {id: 1, name: 'Oliver', status: 'King' },
    {id: 2, name: 'OOliver', status: 'KING' },
    {id: 3, name: 'OliiveRR', status: 'KIIINNNG'  }
  ]

  public arrTwo: any = {
    id: 1, 
    name: 'Oliver', 
    status: 'King'

  }

  public arrThree: any [] = [{
    id: 1,
    name: 'Oliver',
    status: 'King'
  },
  {
    id: 2,
    name: 'OOliver',
    status: 'KING'
  },
  {
    id: 3,
    name: 'OliiveRR',
    status: 'KIIINNNG'
  }
];

}

